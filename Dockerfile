FROM gradle:jdk11 as build
COPY --chown=gradle:gradle . /home/gradle
WORKDIR /home/gradle
RUN gradle clean bootJar -x test -x asciidoctor

FROM adoptopenjdk/openjdk11-openj9:latest
VOLUME /tmp
EXPOSE 8080
COPY --from=build /home/gradle/build/libs/*.jar /healthy-life.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/healthy-life.jar"]
