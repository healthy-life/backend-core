package com.healthy.life.backend.core.repository;

import com.healthy.life.backend.core.entity.UserAchievement;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.springframework.data.jpa.repository.EntityGraph.EntityGraphType.LOAD;

@Repository
public interface UserAchievementRepository extends JpaRepository<UserAchievement, Long> {

    @EntityGraph(attributePaths = "achievement", type = LOAD)
    List<UserAchievement> findAllByUserId(Long userId);

}
