package com.healthy.life.backend.core.mapper;

import com.healthy.life.backend.core.dto.RoleDto;
import com.healthy.life.backend.core.entity.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class RoleMapper {

    public abstract RoleDto toDto(Role role);

}
