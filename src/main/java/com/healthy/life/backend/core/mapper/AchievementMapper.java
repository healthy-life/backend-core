package com.healthy.life.backend.core.mapper;

import com.healthy.life.backend.core.dto.AchievementDto;
import com.healthy.life.backend.core.dto.AchievementInfo;
import com.healthy.life.backend.core.entity.Achievement;
import com.healthy.life.backend.core.entity.UserAchievement;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public abstract class AchievementMapper {

    public abstract AchievementDto toDto(Achievement achievement);

    public abstract Achievement toAchievement(AchievementDto achievementDto);

    @Mappings({
            @Mapping(target = "id", source = "userAchievement.achievement.id"),
            @Mapping(target = "name", source = "userAchievement.achievement.name"),
            @Mapping(target = "description", source = "userAchievement.achievement.description"),
            @Mapping(target = "img", source = "userAchievement.achievement.img"),
            @Mapping(target = "goal", source = "userAchievement.achievement.goal")
    })
    public abstract AchievementInfo toAchievementInfo(UserAchievement userAchievement);

    @Mappings({
            @Mapping(target = "received", constant = "false"),
            @Mapping(target = "progress", constant = "0")
    })
    public abstract AchievementInfo toAchievementInfo(Achievement achievement);

}
