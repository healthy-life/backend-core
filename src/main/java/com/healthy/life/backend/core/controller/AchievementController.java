package com.healthy.life.backend.core.controller;

import com.healthy.life.backend.core.dto.AchievementDto;
import com.healthy.life.backend.core.dto.transfer.Create;
import com.healthy.life.backend.core.dto.transfer.Update;
import com.healthy.life.backend.core.service.AchievementService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.healthy.life.backend.core.controller.AchievementController.ACHIEVEMENTS;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(ACHIEVEMENTS)
@RequiredArgsConstructor
public class AchievementController {
    public static final String ACHIEVEMENTS = "/achievements";

    private final AchievementService achievementService;

    @GetMapping
    public ResponseEntity<List<AchievementDto>> getAchievements() {
        return ResponseEntity.ok(achievementService.getAchievements());
    }

    @PostMapping
    public ResponseEntity<Void> saveAchievement(@Validated(Create.class) @RequestBody AchievementDto achievement) {
        achievementService.save(achievement);
        return ResponseEntity.status(CREATED).build();
    }

    @PutMapping
    public ResponseEntity<Void> updateAchievement(@Validated(Update.class) @RequestBody AchievementDto achievement) {
        achievementService.save(achievement);
        return ResponseEntity.status(OK).build();
    }

}
