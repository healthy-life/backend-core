package com.healthy.life.backend.core.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AchievementInfo {

    private Long id;

    private String name;

    private String description;

    private String img;

    private Boolean received;

    private Integer goal;

    private Integer progress;

}
