package com.healthy.life.backend.core.controller;

import com.healthy.life.backend.core.dto.AchievementInfo;
import com.healthy.life.backend.core.dto.UserDto;
import com.healthy.life.backend.core.service.AchievementService;
import com.healthy.life.backend.core.service.UserService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.healthy.life.backend.core.controller.AchievementController.ACHIEVEMENTS;
import static com.healthy.life.backend.core.controller.UserController.USERS;

@RestController
@RequestMapping(USERS)
@RequiredArgsConstructor
public class UserController {
    public static final String USERS = "/users";
    public static final String USER_ID = "/{userId:\\d+}";

    private final UserService userService;
    private final AchievementService achievementService;

    @GetMapping
    @PageableAsQueryParam
    public ResponseEntity<Page<UserDto>> getUsers(@Parameter(hidden = true) Pageable pageable) {
        return ResponseEntity.ok(userService.getUsers(pageable));
    }

    @GetMapping(USER_ID + ACHIEVEMENTS)
    public ResponseEntity<List<AchievementInfo>> getUserAchievements(@PathVariable("userId") Long userId) {
        return ResponseEntity.ok(achievementService.getUserAchievements(userId));
    }

}
