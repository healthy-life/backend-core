package com.healthy.life.backend.core.dto;

import com.healthy.life.backend.core.dto.transfer.Create;
import com.healthy.life.backend.core.dto.transfer.Update;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Getter
@Setter
public class AchievementDto {

    @Null(groups = Create.class)
    @NotNull(groups = Update.class)
    private Long id;

    @NotBlank(groups = {Create.class, Update.class})
    private String name;

    @NotBlank(groups = {Create.class, Update.class})
    private String description;

    @NotBlank(groups = {Create.class, Update.class})
    private String img;

    @NotNull(groups = {Create.class, Update.class})
    private Integer goal;

}
