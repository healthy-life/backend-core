package com.healthy.life.backend.core.service;

import com.healthy.life.backend.core.dto.UserDto;
import com.healthy.life.backend.core.mapper.UserMapper;
import com.healthy.life.backend.core.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public Page<UserDto> getUsers(Pageable pageable) {
        return userRepository.findAll(pageable).map(userMapper::toDto);
    }

    public Page<UserDto> getUsersByRoleId(Integer roleId, Pageable pageable) {
        return userRepository.findAllByRolesId(roleId.longValue(), pageable).map(userMapper::toDto);
    }

}
