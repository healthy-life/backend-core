package com.healthy.life.backend.core.exception;

import com.healthy.life.backend.core.exception.custom.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

import static java.lang.String.format;
import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.joining;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class ControllerHandleException {

    @ExceptionHandler({MethodArgumentNotValidException.class})
    protected ResponseEntity<ExceptionResponse> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        String msg = ex.getBindingResult().getFieldErrors()
                .stream()
                .map(error -> format("%s: %s", error.getField(), error.getDefaultMessage()))
                .filter(StringUtils::isNotEmpty)
                .collect(joining(", "));

        return ResponseEntity.status(BAD_REQUEST).body(new ExceptionResponse(msg));
    }

    @ExceptionHandler({ResourceNotFoundException.class})
    protected ResponseEntity<ExceptionResponse> handleResourceNotFoundException(ResourceNotFoundException ex) {
        return ResponseEntity.status(NOT_FOUND).body(new ExceptionResponse(ex));
    }

    @Getter
    @AllArgsConstructor
    private static class ExceptionResponse {
        private final LocalDateTime timestamp;
        private final String message;

        public ExceptionResponse(Exception e) {
            this.timestamp = now();
            this.message = e.getMessage();
        }

        public ExceptionResponse(String message) {
            this.timestamp = now();
            this.message = message;
        }

    }

}
