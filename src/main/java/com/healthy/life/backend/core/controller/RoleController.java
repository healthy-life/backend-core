package com.healthy.life.backend.core.controller;

import com.healthy.life.backend.core.dto.RoleDto;
import com.healthy.life.backend.core.dto.UserDto;
import com.healthy.life.backend.core.service.RoleService;
import com.healthy.life.backend.core.service.UserService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.healthy.life.backend.core.controller.RoleController.ROLES;

@RestController
@RequestMapping(ROLES)
@RequiredArgsConstructor
public class RoleController {
    public static final String ROLES = "/roles";
    public static final String ROLE_ID = "/{roleId:\\d+}";

    private final UserService userService;
    private final RoleService roleService;

    @GetMapping
    public ResponseEntity<List<RoleDto>> getRoles() {
        return ResponseEntity.ok(roleService.getRoles());
    }

    @GetMapping(ROLE_ID)
    @PageableAsQueryParam
    public ResponseEntity<Page<UserDto>> getUsersByRoleId(@PathVariable("roleId") Integer id,
                                                          @Parameter(hidden = true) Pageable pageable) {
        return ResponseEntity.ok(userService.getUsersByRoleId(id, pageable));
    }

}
