package com.healthy.life.backend.core.service;

import com.healthy.life.backend.core.dto.AchievementDto;
import com.healthy.life.backend.core.dto.AchievementInfo;
import com.healthy.life.backend.core.mapper.AchievementMapper;
import com.healthy.life.backend.core.repository.AchievementRepository;
import com.healthy.life.backend.core.repository.UserAchievementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class AchievementService {
    private final AchievementRepository achievementRepository;
    private final UserAchievementRepository userAchievementRepository;
    private final AchievementMapper achievementMapper;

    public List<AchievementDto> getAchievements() {
        return achievementRepository.findAll()
                .stream()
                .map(achievementMapper::toDto)
                .collect(toList());
    }

    public List<AchievementInfo> getUserAchievements(@NonNull Long userId) {
        Map<Long, AchievementInfo> achInfoMap = userAchievementRepository.findAllByUserId(userId)
                .stream()
                .map(achievementMapper::toAchievementInfo)
                .collect(toMap(AchievementInfo::getId, achievementInfo -> achievementInfo));

        return achievementRepository.findAll()
                .stream()
                .map(achievementDto -> {
                    AchievementInfo achievementInfo = achInfoMap.get(achievementDto.getId());
                    if (isNull(achievementInfo)) return achievementMapper.toAchievementInfo(achievementDto);
                    else return achievementInfo;
                })
                .collect(toList());
    }

    @Transactional
    public void save(@NonNull AchievementDto achievementDto) {
        achievementRepository.saveAndFlush(achievementMapper.toAchievement(achievementDto));
    }

}
