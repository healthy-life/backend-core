package com.healthy.life.backend.core.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

    private Long id;

    private String username;

    private String password;

    private String email;

    private String img;

    private String country;

    private String gender;

}
