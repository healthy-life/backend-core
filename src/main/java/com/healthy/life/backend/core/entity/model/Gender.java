package com.healthy.life.backend.core.entity.model;

public enum Gender {
    MALE,
    FEMALE
}
