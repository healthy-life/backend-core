package com.healthy.life.backend.core.mapper;

import com.healthy.life.backend.core.dto.UserDto;
import com.healthy.life.backend.core.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    public abstract UserDto toDto(User user);

}
