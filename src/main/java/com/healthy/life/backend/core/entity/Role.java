package com.healthy.life.backend.core.entity;

import com.healthy.life.backend.core.entity.model.RoleEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "roles")
@EqualsAndHashCode(callSuper = true)
public class Role extends BaseEntity {

    @Enumerated(STRING)
    @Column(name = "name")
    private RoleEnum name;

}
