package com.healthy.life.backend.core.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.FetchType.LAZY;

@Data
@Entity
@Table(name = "users_achievements")
@EqualsAndHashCode(callSuper = true)
public class UserAchievement extends BaseEntity {

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "achievement_id")
    private Achievement achievement;

    @Column(name = "received")
    private Boolean received;

    @Column(name = "progress")
    private Integer progress;

}
