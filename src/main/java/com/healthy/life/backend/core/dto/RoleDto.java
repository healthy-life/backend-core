package com.healthy.life.backend.core.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleDto {

    private Integer id;

    private String name;

}
