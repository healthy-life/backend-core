CREATE TABLE users_roles
(
    id      BIGSERIAL NOT NULL,
    user_id BIGINT    NOT NULL,
    role_id BIGINT    NOT NULL,
    CONSTRAINT users_roles_pkey PRIMARY KEY (id),
    CONSTRAINT users_fkey FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT roles_fkey FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE
);