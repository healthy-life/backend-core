CREATE TABLE achievements
(
    id          BIGSERIAL    NOT NULL,
    name        VARCHAR(50)  NOT NULL UNIQUE,
    description varchar(255) NOT NULL,
    img         VARCHAR(255) NOT NULL,
    goal        INTEGER      NOT NULL,
    CONSTRAINT achievements_pkey PRIMARY KEY (id)
);