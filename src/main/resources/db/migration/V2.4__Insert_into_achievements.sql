INSERT INTO achievements (name, description, img, goal)
VALUES ('First 1000m', 'Run your first 1000m',
        'https://cdn.pixabay.com/photo/2015/07/15/11/49/success-846055_960_720.jpg', 1000);
INSERT INTO achievements (name, description, img, goal)
VALUES ('First 10000m', 'Run your first 10000m',
        'https://cdn.pixabay.com/photo/2015/07/15/11/49/success-846055_960_720.jpg', 10000);