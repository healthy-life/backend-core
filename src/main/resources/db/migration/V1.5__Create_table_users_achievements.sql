CREATE TABLE users_achievements
(
    id             BIGSERIAL NOT NULL,
    user_id        BIGINT    NOT NULL,
    achievement_id BIGINT    NOT NULL,
    received       BOOLEAN   NOT NULL DEFAULT FALSE,
    progress       INTEGER   NOT NULL DEFAULT 0,
    CONSTRAINT users_achievements_pkey PRIMARY KEY (id),
    CONSTRAINT users_fkey FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT achievements_fkey FOREIGN KEY (achievement_id) REFERENCES achievements (id) ON DELETE CASCADE
);