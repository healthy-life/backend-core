CREATE TABLE users
(
    id         BIGSERIAL   NOT NULL,
    username   VARCHAR(50) NOT NULL UNIQUE,
    password   VARCHAR(255),
    email      VARCHAR(100) UNIQUE,
    first_name VARCHAR(100),
    last_name  VARCHAR(100),
    img        VARCHAR(255),
    country    VARCHAR(50),
    gender     VARCHAR(10),
    CONSTRAINT users_pkey PRIMARY KEY (id)
);